import XCTest
import Alamofire

class NagivationEngineTests: XCTestCase {
    func testPathFinding() {
        let apiUrl = Config.apiURL
        let apiUrlWithEndpoint = apiUrl + "/floor/1"
        let navigationEngine = NavigationEngine()
        let firstPoint = Point(id: 0, x: 2, y: 0, obstructionID: 0)
        let secondPoint = Point(id: 0, x: 9, y: 3, obstructionID: 0)

        let expected = [
            NavigationInstruction(distance: 4, relativeDirection: .forward),
            NavigationInstruction(distance: 4, relativeDirection: .right),
            NavigationInstruction(distance: 3, relativeDirection: .left),
            NavigationInstruction(distance: 3, relativeDirection: .right),
            NavigationInstruction(distance: 4, relativeDirection: .right)
        ]

        // This ensures that the asynchronous call can complete before the test ends.
        let e = expectation(description: "Alamofire")

        request(apiUrlWithEndpoint).responseJSON { response in
            if let data = response.data {
                let jsonDecoder = JSONDecoder()
                do {
                    let floor = try jsonDecoder.decode(Floor.self, from: data)
                    navigationEngine.addMap(with: floor)
                    let instructions = navigationEngine.buildNavigationInstructions(from: firstPoint, to: secondPoint, for: floor, facing: .north)

                    if let instructions = instructions {
                        for instruction in instructions {
                            print("instruction: move \(instruction.distance) to the \(instruction.relativeDirection)")
                        }

                        XCTAssertEqual(expected, instructions)
                    }
                } catch {
                    print("Could not decode data to floor.")
                }

                e.fulfill()
            }
        }

        waitForExpectations(timeout: 5.0, handler: nil)
    }
}
