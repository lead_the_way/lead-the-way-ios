# Lead the Way

An indoor navigation app for the visually impaired.

# Install (macOS)

Lead the Way uses CocoaPods for managing dependencies. Therefore CocoaPods must be installed first:

```
gem install cocoapods
```

In order to property setup Lead the Way, simply clone the Git repository and install the dependencies (pods).

```
git clone git@gitlab.com:lead_the_way/lead-the-way-ios.git
cd lead-the-way-ios
pod install
```

Since Lead the Way uses CocoaPods, the `leadtheway-ios.xcworkspace` file should be used to launch Xcode (NOT the `leadtheway-ios.xcodeproj` file).

# Building and Running

Lead the Way has two schemes from which to choose:

* **leadtheway-ios**: This scheme will use the **production** database and API ([api.leadtheway.app](api.leadtheway.app))
* **leadtheway-ios Developer**: This scheme will use the **development** database and API ([dev.leadtheway.app](dev.leadtheway.app))

If `leadtheway-ios Developer` doesn't show up as a scheme from which to choose, then in Xcode click on `Product -> Scheme -> New Scheme...`, ensure that the Target is set to `leadtheway-ios Developer`, and then click OK.

Once the correct scheme is chosen click on the Play button (or alternatively click on `Production -> Run`).

# Setup for SwiftLint

[SwiftLint](https://github.com/realm/SwiftLint) is a linter built for Swift that can automatically check for potential formatting errors in a Swift project.
If any problems occur when SwiftLint is expected to run, it may be necessary to set it up again.
* Ensure that the project's `.xcworkspace` file is opened and not the `.xcodeproj` file
* In the Project Navigator on the left-hand side of Xcode, highlight the project `leadtheway-ios`
* Ensure that a target is selected in the main view (i.e. `leadtheway-ios` or `leadtheway-ios Development`)
* Select `Build Phases` from the vertical menu near the top of the main view
* From the top menu bar click `Editor -> Add Build Phase -> Add Run Script Build Phase`
* In the main view expand the `Run Script` section
* In the `/bin/sh` section, replace the available code with the following:
  * `"${PODS_ROOT}/SwiftLint/swiftlint"`
* SwiftLint should now evaluate the code every time the project is built

# Building and Running Problems

If there are errors about dependencies then the problem is likely that the .xcodeproj file was used instead of the .xcworkspace file; close Xcode and open the correct file to try running again.

If there are seemingly missing features or other versioning problems, ensure that the correct branch is checked out in Git.

If problems persist, then try cleaning the build folder by clicking `Product -> Clean Build Folder`. This will fix a lot of common problems in Xcode.
