import Foundation
import CoreLocation

/// Delegate needed for `CoreLocation` and `CLLocationManager`.
class LocationDelegate: NSObject, CLLocationManagerDelegate {
    // MARK: Properties

    var headingCallback: ((CLLocationDirection) -> ())? = nil

    // MARK: Instance Methods

    func locationManager(_ manager: CLLocationManager, didUpdateHeading newHeading: CLHeading) {
        headingCallback?(newHeading.trueHeading)
    }
}
