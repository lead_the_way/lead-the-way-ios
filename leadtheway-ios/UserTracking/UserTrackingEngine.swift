import Foundation
import CoreMotion
import CoreLocation
import UIKit

/// The core engine responsible for tracking the user live as they move around.
class UserTrackingEngine {
    // Gives access to CoreMotion functions.
    let motionManager = CMMotionManager()

    // Gives access to CoreLocation functions.
    let locationManager = CLLocationManager()

    // Delegates location tasks to a different thread.
    let locationDelegate = LocationDelegate()

    // Tracks the latest heading detected, with the offset applied.
    private var latestHeading = 0.0

    /// Set to true when a user is turning, false when user is facing a specific direction.
    var isUserTurning = false

    /// Set to true when the user is in the process of taking a step.
    var isUserStepping = false

    /// Variable for changing to adapt to a user's score.
    var userMotionScore = 0.25

    /// Tracks when the motion score should be updated to adjust to a user's personal settings.
    var updateMotionScoreTimer = 5

    /// Tracks the max motion score that a user accumulates over several steps.
    var maxMotionScore = 0.0

    /// Variables used for keeping track of the user's current location.
    var currentLocation = (x: 0, y: 0)

    /// Variable that holds the instructions for the user to follow.
    var instructions: [NavigationInstruction]?

    /// Variable to keep track of the current instruction.
    var currentInstruction = 0

    // MARK: NotificationCenter methods.
    
    func sendCurrentInstructionUpdatedNotification(_ navigationInstruction: NavigationInstruction) {
        // Setup a notification for when the current instruction changes.
        NotificationCenter.default.post(name: Notification.Name(rawValue: "CurrentInstructionUpdated"), object: navigationInstruction)
    }

    func sendCurrentLocationUpdatedNotification(with currentLocation: String) {
        NotificationCenter.default.post(name: Notification.Name(rawValue: "CurrentLocationUpdated"),
                                        object: currentLocation)
    }

    func sendArrivedNotification() {
        NotificationCenter.default.post(name: Notification.Name(rawValue: "Arrived"),
                                        object: currentLocation)
    }

    // MARK: UserTrackingEngine methods.

    /// Filter device data.
    /// Uses accelerometer and gyroscope values to accurately
    /// track when a user has taken a step.
    func filterDeviceData(deviceMotion: CMDeviceMotion) {
        // Filter for a user turning in order to reduce false steps in a turn.

        // Filter for taking a step.
        // Currently uses a hard coded step threshold to determine when a user has taken a step.
        // Using the gyrometer, some of the device motion can be canceled out.
        // For example:
        //
        // The user is stationary, but is tilting the phone to get a better look
        // at the screen. The acceleration is similar to that of a step, and the
        // app thinks the user has moved.
        //
        // By detecting that the phone is rotating and subtracting
        // a percentage value from the phones acceleration, such false
        // positives can be greatly reduced.
        let currentAccX = deviceMotion.userAcceleration.x
        let currentAccY = deviceMotion.userAcceleration.y
        let currentAccZ = deviceMotion.userAcceleration.z

        let currentRotX = deviceMotion.rotationRate.x
        let currentRotY = deviceMotion.rotationRate.y
        let currentRotZ = deviceMotion.rotationRate.z

        let currentTurnScore = currentRotZ + currentRotY

//        print("Current Turning Score: \(currentTurnScore)")

        // If a user was not previously turning, see if they have started a turn.
        if isUserTurning == false {
            if abs(currentTurnScore) > 0.5 {
                isUserTurning = true
            }
        }
        // If a user is turning, see if they have stopped turning.
        if isUserTurning == true {
            if abs(currentTurnScore) <= 0.5 {
                isUserTurning = false
            }
        }

        // After having checked both cases, if the user is turning, return.
        if isUserTurning == true {
//            print("Turning")
            return
        }

        // Calculate movement score.
        var accelerationScore = 0.0
        if abs(currentAccZ) > abs(currentAccY) {
            accelerationScore = (currentAccZ+currentAccY)*min(0.7+currentAccY/currentAccZ, 1.0)
        }
        else {
            accelerationScore = (currentAccZ+currentAccY)*min(0.7+currentAccZ/currentAccY, 1.0)
        }
        //print("AccZ: \(currentAccZ), AccY: \(currentAccY), accScore: \(accelerationScore)")
        let currentMotionScore = (accelerationScore)*1.2 - 0.1*abs(currentRotY + currentRotZ) - 0.1*abs(currentRotX)

        // Update the max motion score.
        if currentMotionScore > maxMotionScore {
            maxMotionScore = currentMotionScore
        }

//        print("Current Score: \(currentMotionScore)")

        // Set the user motion score after some data has been gathered.
        if updateMotionScoreTimer <= 0 {
            userMotionScore = 0.25
        }

        // See if is taking a step. Uses peaks.
        if currentMotionScore > userMotionScore && isUserStepping == false {
            // Set the boolean to true to 'stop' motion capture.
            isUserStepping = true

            // Update the 'timer' for when to update the motion score to fit a user.
            updateMotionScoreTimer -= 1

            // Call the handler function for a step.
            print("Step Taken")
            processStepTaken()
        }

        // See if the step has completed in order to record another step.
        else if currentMotionScore <= userMotionScore && isUserStepping == true {
            isUserStepping = false
        }
    }

    /// Processes which way a user is now moving if a step event has been fired off.
    func processStepTaken() {
        // Calculate the heading direction to be N, S, E, or W,
        // and add the appropriate distance to the direction of the heading.
        if latestHeading > 315 || latestHeading <= 45 {
            // North headig.
            currentLocation.y += 2
        } else if latestHeading > 45 && latestHeading <= 135 {
            // East heading.
            currentLocation.x += 2
        } else if latestHeading > 135 && latestHeading <= 225 {
            // South heading.
            currentLocation.y -= 2
        } else if latestHeading > 225 && latestHeading <= 315 {
            // West heading.
            currentLocation.x -= 2
        }

        // Send the new current location.
        sendCurrentLocationUpdatedNotification(with: "\(currentLocation)")

        // After each step has been taken, determine if the
        // current instruction has been fulfilled.
        checkInstruction()
    }

    func getCardinalDirection() -> CardinalDirection {
        if latestHeading > 315 || latestHeading <= 45 {
            return .north
        } else if latestHeading > 45 && latestHeading <= 135 {
            return .east
        } else if latestHeading > 135 && latestHeading <= 225 {
            return .south
        } else {
            return .west
        }
    }

    /// Function to check if the current instruction has been followed.
    /// Will check if the route has been completed and stop user tracking.
    /// Will move on to the next instruction if one is done.
    func checkInstruction() {
        // Using cardinal heading and distance, determine the point where user should be.
        let instructionDistance = self.instructions?[currentInstruction].distance
        let instructionHeading = self.instructions?[currentInstruction].cardinalDirection

        // Similar case to findind the heading for a step taken.
        // This case determines the sign and value of the direction that the user should be moving.
        // Then each case is compared, and if the value is in line with the instruction,
        // we move to the next instruction and fire a instruction complete event.
        if instructionHeading == CardinalDirection.north {
            // North headig.
            if currentLocation.y >= instructionDistance! {
                instructionComplete()
            }
        } else if instructionHeading == CardinalDirection.east  {
            // East heading.
            if currentLocation.x >= instructionDistance! {
                instructionComplete()
            }
        } else if instructionHeading == CardinalDirection.south  {
            // South heading.
            if currentLocation.y <= -instructionDistance! {
                instructionComplete()
            }
        } else if instructionHeading == CardinalDirection.west  {
            // West heading.
            if currentLocation.x <= -instructionDistance! {
                instructionComplete()
            }
        }

    }

    /// Function to move to next instruction.
    /// Checks for trip complete.
    /// Includes UI handlers.
    /// Resets global currentLocation variable to 0,0
    func instructionComplete() {
        // Move to next instruction.
        self.currentInstruction += 1

        // See if done with the trip.
        if self.currentInstruction >= (self.instructions?.count)! {
            // Stop user tracking.
            self.stopUserTracking()

            // Fire trip complete event for UI.
            sendArrivedNotification()

            // Return, as there's nothing left to do.
            return
        }

        // We are not done with the trip, fire next instruction UI event,
        // and reset the currentLocation variable.
        self.currentLocation.x = 0
        self.currentLocation.y = 0

        // Send the new current instruction.
        if let currentInstruction = self.instructions?[optional: currentInstruction] {
            sendCurrentInstructionUpdatedNotification(currentInstruction)
        }
    }

    /// Starts gathering heading data.
    /// Required in order to get live heading data.
    func startHeadingUpdates() {
        // CoreLocation portion of the user tracking engine.

        // Request authorization to use CoreLocation.
        // This is only required by CoreLocation services.
        locationManager.requestWhenInUseAuthorization()

        // Start the gathering of heading data.
        locationManager.startUpdatingHeading()

        // Delegate location updates to seperate thread.
        locationManager.delegate = locationDelegate

        // Create the heading callback function.
        locationDelegate.headingCallback = { newHeading in
            // Set global variable to update.
            self.latestHeading = newHeading
            print(self.latestHeading)
        }
    } // End of startHeadingUpdates().

    /// Starts user tracking.
    /// Main call function to start the engine.
    func startUserTracking() {
        guard instructions == instructions else {
            print("ERROR: Instructions are nil when trying to start user tracking.")
            return
        }

        isUserTurning = false
        isUserStepping = false
        currentInstruction = 0

        // Start heading updates.
        self.startHeadingUpdates()

        // CoreMotion portion of the engine.

        // Check to make sure that CoreMotion is available.
        guard motionManager.isDeviceMotionAvailable else {print("Motion Data is not available");  return }

        // Set the update rate of CoreMotion data.
        motionManager.deviceMotionUpdateInterval = 1.0/20.0

        // Start the Motion updates, with Z being the vertical axis.
        // Set the callback function.
        motionManager.startDeviceMotionUpdates(using: .xArbitraryZVertical, to: .main) { deviceMotion, error in
            guard let deviceMotion = deviceMotion else { return }

            // Send motion data to filter function.
            self.filterDeviceData(deviceMotion: deviceMotion)
        }

        // This should be the start, i.e. (0, 0)
        currentLocation = (x: 0, y: 0)
        sendCurrentLocationUpdatedNotification(with: "\(currentLocation)")
    }

    /// Stops all tracking.
    func stopUserTracking() {
        locationManager.stopUpdatingHeading()
        motionManager.stopDeviceMotionUpdates()

        instructions = nil
    } // End of stopUserTracking().

    /// Loads instructions to follow.
    /// MUST be called before starting userTracking.
    func loadInstructions(instructions: [NavigationInstruction]) {
        // Load the global variable
        self.instructions = instructions

        // Send the first instruction.
        if let currentInstruction = self.instructions?[optional: 0] {
            sendCurrentInstructionUpdatedNotification(currentInstruction)
        }
    }
}
