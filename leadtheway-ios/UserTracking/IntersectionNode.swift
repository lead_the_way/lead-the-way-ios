import Foundation

/// A node (2D graph point) that represents an intersection point on a graph.
///
/// - Note: An intersection point is the point at which a path changes direction.
struct IntersectionNode {
    let x: Int
    let y: Int
}
