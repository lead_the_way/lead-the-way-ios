import Foundation

/// Static strings for the user interface.
struct Strings {
    static let changeToProduction = "Change to Production"
    static let changeToDevelopment = "Change to Development"
    
    static let successfullyDeleted = "Successfully deleted."
}
