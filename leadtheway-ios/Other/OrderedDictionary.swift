import Foundation

typealias OrderedDictionary = [(key: String, value: [String])]

extension OrderedDictionary {
    func keyCount() -> Int {
        return self.count
    }

    func valueCount(for key: Int) -> Int {
        return self[key].value.count
    }

    func getKey(at index: Int) -> String {
        return self[index].key
    }

    func getValues(at index: Int) -> [String] {
        return self[index].value
    }

    func getValue(withKey key: Int, at value: Int) -> String {
        return self[key].value[value]
    }

    mutating func addPair(_ key: String, _ value: String) {
        // Try to find a key that matches...
        for i in 0..<self.count {
            if self[i].key == key {
                self[i].value.append(value)
                return
            }
        }

        // If no key matches, create it.
        self.append((key, [value]))
    }
}
