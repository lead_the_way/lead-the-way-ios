import Foundation

/// Holds static configuration variables that are dependent on if the target has the Swift flag "-DDEVELOPMENT" set.
struct Config {

    // MARK: API Constants

    #if DEVELOPMENT
    static let apiURL = "https://dev.leadtheway.app" // Development API
    #else
    static let apiURL = "https://api.leadtheway.app" // Production API
    #endif

    // MARK: Endpoint Constants

    static let building = "building"
    static let within = "within"
    static let floor = "floor"

    // MARK: Type Methods

    /// Determines if the current target is a development environment.
    ///
    /// - Returns: True if development; otherwise false.
    static func isDevelopment() -> Bool {
        #if DEVELOPMENT
        return true
        #else
        return false
        #endif
    }

    /// Determines if the current target is a production environment.
    ///
    /// - Returns: True if production; otherwise false.
    static func isProduction() -> Bool {
        #if DEVELOPMENT
        return false
        #else
        return true
        #endif
    }
}
