import Foundation

public enum RelativeDirection: String {
    case forward = "forward"
    case right = "right"
    case back = "back"
    case left = "left"

    static func convertCardinalDirectionsToRelativeDirection(_ first: CardinalDirection,
                                                             _ second: CardinalDirection) -> RelativeDirection {
        switch first.index() - second.index() {
        case 0:
            return .forward
        case -1, 3:
            return .right
        case -3, 1:
            return .left
        case -2, 2:
            return .back
        default:
            fatalError("Invalid conversion from cardinal directions to relative direction.")
        }
    }
}
