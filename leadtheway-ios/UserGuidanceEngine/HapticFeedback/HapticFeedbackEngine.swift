import Foundation
import AudioToolbox

/**
 Give the user haptic feedback via virations.
 */
public class HapticFeedbackEngine: FeedbackEngine {
    
    /**
     Gives direction for the user to follow using vibrations. This is done asynchronously to not have the main thread
     sleep and hold up other operations.
     - Parameter to: Direction the user will be going.
     - Parameter for: The distance the user will travel.
     */
    public func giveDirection(to direction: RelativeDirection, for distance: Int) {
        DispatchQueue.global(qos: .userInteractive).async {
            switch direction {
            case .left:
                AudioServicesPlaySystemSound(kSystemSoundID_Vibrate)
            case .right:
                for _ in 0..<2 {
                    AudioServicesPlaySystemSound(kSystemSoundID_Vibrate)
                    sleep(1)
                }
            case .forward:
                for _ in 0..<3 {
                    AudioServicesPlaySystemSound(kSystemSoundID_Vibrate)
                    sleep(1)
                }
            case .back:
                for _ in 0..<4 {
                    AudioServicesPlaySystemSound(kSystemSoundID_Vibrate)
                    sleep(1)
                }
            }
        }
    }
}
