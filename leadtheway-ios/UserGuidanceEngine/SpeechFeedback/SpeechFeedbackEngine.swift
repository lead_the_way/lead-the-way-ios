import Foundation
import AVFoundation

/**
 Gives the user speech feedback via speech.    /**
 Gives direction for the user to follow using vibrations. This is done asynchronously to not have the main thread
 sleep and hold up other operations.
 - Parameter to: Direction the user will be going.
 - Parameter for: The distance the user will travel.
 */
 */
public class SpeechFeedbackEngine: FeedbackEngine {
    
    /// This is what alows us to convert text to speech.
    private let speechSynthesizer: AVSpeechSynthesizer
    
    init() {
        speechSynthesizer = AVSpeechSynthesizer()
    }
    
    /**
     Gives direction for the user to follow using vibrations. This is done asynchronously to not have the main thread
     sleep and hold up other operations.
     - Parameter to: Direction the user will be going.
     - Parameter for: The distance the user will travel.
     */
    public func giveDirection(to direction: RelativeDirection, for distance: Int) {
        let textToSpeech = AVSpeechUtterance(string: "Go \(direction) for \(distance) steps")
        speak(textToSpeech)
    }

    public func giveDirection(to direction: RelativeDirection, for distanceInMeters: Double) {
        let textToSpeech = AVSpeechUtterance(string: getString(with: direction, for: distanceInMeters))
        speak(textToSpeech)
    }

    public func arrived() {
        let textToSpeech = AVSpeechUtterance(string: "Arrived!")
        speak(textToSpeech)
    }

    public func announceSpeechRecognition() {
        let textToSpeech = AVSpeechUtterance(string: "Where would you like to go?")
        speak(textToSpeech)
    }

    private func speak(_ textToSpeech: AVSpeechUtterance) {
        speechSynthesizer.stopSpeaking(at: .immediate)
        speechSynthesizer.speak(textToSpeech)
    }
}
