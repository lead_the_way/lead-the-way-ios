import Foundation

/**
 This protocol must be conformed to if making a feedback engine.
 */
public protocol FeedbackEngine {
    func giveDirection(to direction: RelativeDirection, for distance: Int)
}

extension FeedbackEngine {
    func getString(with direction: RelativeDirection, for distanceInMeters: Double) -> String {
        var text: String

        switch direction {
        case .forward:
            text = "Go forward \(distanceInMeters) meter"
        case .right:
            text = "Turn right and go \(distanceInMeters) meter"
        case .back:
            text = "Turn around and go \(distanceInMeters) meter"
        case .left:
            text = "Turn left and go \(distanceInMeters) meter"
        }

        if distanceInMeters != 1.0 {
            text += "s"
        }

        return text
    }
}
