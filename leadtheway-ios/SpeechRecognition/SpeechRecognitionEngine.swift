import Foundation
import UIKit
import Speech

public enum SpeechRecognitionStatus {
    case ready
    case busy
    case listening
    case notAvailable
}

public class SpeechRecognitionEngine: NSObject, SFSpeechRecognizerDelegate {
    public var status = SpeechRecognitionStatus.busy {
        didSet {
            print(status)
        }
    }

    private var speechRecognizer = SFSpeechRecognizer(locale: Locale(identifier: "en-US"))
    private var recognitionRequest: SFSpeechAudioBufferRecognitionRequest?
    private var recognitionTask: SFSpeechRecognitionTask?
    private var audioEngine = AVAudioEngine()

    private var timer: Timer?

    override init() {
        super.init()
        speechRecognizer?.delegate = self

        SFSpeechRecognizer.requestAuthorization { authStatus in
            switch authStatus {
            case .authorized:
                self.status = .ready
            case _:
                self.status = .notAvailable
            }
        }
    }

    public func isRunning() -> Bool {
        return audioEngine.isRunning
    }

    public func startSpeechRecognition(_ handler: @escaping (_ destination: String?, _ destinationType: String?) -> ()) throws {
        print("Starting speech recognition...")

        // Cancel the previous task if it is running.
        if let recognitionTask = recognitionTask {
            recognitionTask.cancel()
            self.recognitionTask = nil
        }

        let audioSession = AVAudioSession.sharedInstance()
//        try audioSession.setCategory(AVAudioSession.Category.record, mode: AVAudioSession.Mode.measurement, options: [])
        try audioSession.setCategory(.playAndRecord, mode: .default, options: [.defaultToSpeaker])
        try audioSession.setActive(true, options: .notifyOthersOnDeactivation)

        recognitionRequest = SFSpeechAudioBufferRecognitionRequest()

        let inputNode = audioEngine.inputNode

        guard let recognitionRequest = recognitionRequest else {
            fatalError("Unable to created a SFSpeechAudioBufferRecognitionRequest object.")
        }

        // Configure request so that results are returned before audio recording is finished.
        recognitionRequest.shouldReportPartialResults = true

        // A recognition task represents a speech recognition session.
        // We keep a reference to the task so that it can be cancelled.
        recognitionTask = speechRecognizer?.recognitionTask(with: recognitionRequest) { result, error in
            var isFinal = false

            if let result = result {
                isFinal = result.isFinal
            }

            if error != nil || isFinal {
                self.audioEngine.stop()
                inputNode.removeTap(onBus: 0)

                self.recognitionRequest = nil
                self.recognitionTask = nil

                if let transcription = result?.bestTranscription.formattedString {
                    let (resultingDestination, resutlingDestinationType) = self.performSpeechAction(with: transcription)
                    handler(resultingDestination, resutlingDestinationType)
                }
            } else if error == nil {
                self.restartSpeechTimer()
            }
        }

        let recordingFormat = inputNode.outputFormat(forBus: 0)
        inputNode.installTap(onBus: 0, bufferSize: 1024, format: recordingFormat) {
            (buffer: AVAudioPCMBuffer, when: AVAudioTime) in
            self.recognitionRequest?.append(buffer)
        }

        audioEngine.prepare()

        try audioEngine.start()
    }

    private func restartSpeechTimer() {
        timer?.invalidate()
        timer = Timer.scheduledTimer(withTimeInterval: 1.5, repeats: false) { timer in
            if self.audioEngine.isRunning {
                self.stopSpeechRecording()
            }
        }
    }

    public func stopSpeechRecording() {
        print("Stopping speech recognition...")

        audioEngine.inputNode.removeTap(onBus: 0)
        audioEngine.stop()
        recognitionRequest?.endAudio()
    }

    private func performSpeechAction(with transcription: String) -> (String?, String?) {
        var startLocation: String?
        var endLocation: String?
        var transcription = transcription.lowercased()
        let sentence = transcription.split(separator: " ")

        print("TRANSCRIPTION: \(transcription)")

        if transcription.contains("start") {
            if let startLocationWord = sentence[optional: 2] {
                startLocation = String(startLocationWord)
                endLocation = String(sentence[sentence.count - 1])
            } else {
                print("ERROR: Sentence out of bounds.")
            }
        } else if transcription.contains("route") {
            endLocation = String(sentence[sentence.count - 1])
        } else {
            print("Did not detect proper locations.")
        }

        return (startLocation, endLocation)
    }

}
