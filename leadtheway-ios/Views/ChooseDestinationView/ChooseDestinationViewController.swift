import UIKit

class ChooseDestinationViewController: UITableViewController {
    var destinations: OrderedDictionary = []
    var startLocation: String?
    var endLocation: String?

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return destinations.keyCount()
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return destinations.valueCount(for: section)
    }

    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        var title = destinations.getKey(at: section)
        title = title.capitalized

        // If the word ends in "y" then change it to "ies"
        // Example: Entry -> Entries
        if title.last == "y" {
            title.removeLast()
            title += "ies"
        // If the word does not already end in "s" then add it
        // Example: Room -> Rooms
        // Example: Stairs -> Stairs (no change)
        } else if title.last != "s" {
            title += "s"
        }

        return title
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DestinationCell", for: indexPath)

        cell.textLabel?.text = destinations.getValue(withKey: indexPath.section, at: indexPath.row)

        return cell
    }

    override func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        // Get the selection value of the row that was tapped again.
        let selection = destinations.getValue(withKey: indexPath.section, at: indexPath.row)

        // If the selection matches the current startLocation, make the startLocation nil.
        // This essentially "deselects" the choice and starts fresh.
        if startLocation == selection {
            startLocation = nil
        }
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // Get the selection value to send back to the `NavigationViewController`.
        let selection = destinations.getValue(withKey: indexPath.section, at: indexPath.row)

        if startLocation == nil {
            startLocation = selection
            return
        } else {
            endLocation = selection
        }

        // Hang on to the reference of the presenting `NavigationViewController` because `self` no longer exists by the
        // time that the completion block in `dismiss(animated:completion:)` is executed.
        let presentingViewController = self.presentingViewController

        // Dismiss this view controller and set the selected value on the `NavigationViewController`.
        dismiss(animated: true) {
            if let presentingViewController = presentingViewController as? NavigationViewController,
                let startLocation = self.startLocation, let endLocation = self.endLocation {
                presentingViewController.route(from: startLocation, to: endLocation)
            }
        }
    }

    @IBAction func cancelButtonPressed(_ sender: UIBarButtonItem) {
        dismiss(animated: true)
    }
}
