import SpriteKit
import GameplayKit

class MapSKScene: SKScene {
    var map: Map?
    let cameraNode = SKCameraNode()
    var previousCameraPoint = CGPoint.zero

    @nonobjc var spriteNodes = [[SKSpriteNode?]]()

    func setup(with map: Map) {
        self.map = map
    }

    func start() {
        generateMazeNodes()
    }

    func unwrapMap() -> Map {
        guard let map = map else {
            fatalError("GameScene.setup(with:) must be called first.")
        }

        return map
    }

    func route(from startPoint: Point, to endPoint: Point) {
        let map = unwrapMap()

        if let solution = map.getGridPath(from: startPoint, to: endPoint) {
            animateSolution(solution, from: startPoint, to: endPoint)
        } else {
            print("Could not get solution.")
        }
    }

    /// Generates a maze when the game starts.
    override func didMove(to _: SKView) {
        let panGesture = UIPanGestureRecognizer()
        panGesture.addTarget(self, action: #selector(panGestureAction(_:)))
        view?.addGestureRecognizer(panGesture)

        cameraNode.position = CGPoint(x: size.width / 2, y: size.height / 2)
        addChild(cameraNode)
        camera = cameraNode
    }

    @objc func panGestureAction(_ sender: UIPanGestureRecognizer) {
        // The camera has a weak reference, so test it
        guard let camera = self.camera else {
            return
        }
        // If the movement just began, save the first camera position
        if sender.state == .began {
            previousCameraPoint = camera.position
        }
        // Perform the translation
        let translation = sender.translation(in: self.view)
        let newPosition = CGPoint(
            x: previousCameraPoint.x + translation.x * -1,
            y: previousCameraPoint.y + translation.y
        )
        camera.position = newPosition
    }

    /// Generates sprite nodes that comprise the maze's visual representation.
    func generateMazeNodes() {
        let map = unwrapMap()

        let mapWidth = map.graph.gridWidth // Floor.width
        let mapHeight = map.graph.gridHeight // Floor.depth

        // Initialize the an array of sprites for the maze.
        spriteNodes += [[SKSpriteNode?]](repeating: [SKSpriteNode?](repeating: nil, count: mapHeight), count: mapWidth)

        /*
         Grab the maze's parent node from the scene and use it to
         calculate the size of the maze's cell sprites.
         */
        guard let mazeParentNode = childNode(withName: "map") as? SKSpriteNode else {
            fatalError("mazeParentNode could not be casted.")
        }

        let cellDimension = mazeParentNode.size.height / CGFloat(mapWidth > mapHeight ? mapWidth : mapHeight)

        // Remove existing maze cell sprites from the previous maze.
        mazeParentNode.removeAllChildren()

        // For each maze node in the maze graph, create a corresponding sprite.
        let graphNodes = map.graph.nodes as? [GKGridGraphNode]
        for node in graphNodes! {
            // Get the position of the maze node.
            let x = Int(node.gridPosition.x)
            let y = Int(node.gridPosition.y)

            if (map.graph.node(atGridPosition: vector_int2(Int32(x), Int32(y))) == nil) {
                continue
            }

            /*
             Create a maze sprite node and place the sprite at the correct
             location relative to the maze's parent node.
             */
            let mazeNode = SKSpriteNode(
                color: SKColor.darkGray,
                size: CGSize(width: cellDimension, height: cellDimension)
            )
            mazeNode.anchorPoint = CGPoint(x: 0, y: 0)
            mazeNode.position = CGPoint(x: CGFloat(x) * cellDimension, y: CGFloat(y) * cellDimension)

            // Add the maze sprite node to the maze's parent node.
            mazeParentNode.addChild(mazeNode)

            /*
             Add the maze sprite node to the 2D array of sprite nodes so we
             can reference it later.
             */
            spriteNodes[x][y] = mazeNode
        }
    }

    func clearPreviousPath() {
        var count = 0

        spriteNodes.forEach {
            $0.forEach {
                if $0?.color != .darkGray || $0?.color != .black {
                    $0?.color = .darkGray
                    count += 1
                }
            }
        }

        print("\(count) nodes changed.")
    }

    /// Animates a solution to the maze.
    func animateSolution(_ solution: [GKGridGraphNode], from startPoint: Point, to endPoint: Point) {
        clearPreviousPath()

        // Color the start and end nodes green and red, respectively.
        spriteNodes[startPoint.x][startPoint.y]?.color = SKColor.green
        spriteNodes[endPoint.x][endPoint.y]?.color     = SKColor.red
        
        /*
         The animation works by animating sprites with different start delays.
         actionDelay represents this delay, which increases by
         an interval of actionInterval with each iteration of the loop.
         */
        var actionDelay: TimeInterval = 0
        let actionInterval = 0.005

        /*
         Light up each sprite in the solution sequence, except for the
         start and end nodes.
         */
        guard solution.count - 2 > 0 else {
            print("ERROR: Could not process the start and end nodes.")
            return
        }

        for i in 1...(solution.count - 2) {
            // Grab the position of the maze graph node.
            let x = Int(solution[i].gridPosition.x)
            let y = Int(solution[i].gridPosition.y)

            /*
             Increment the action delay so this sprite is highlighted
             after the previous one.
             */
            actionDelay += actionInterval

            // Run the animation action on the maze sprite node.
            if let mazeNode = spriteNodes[x][y] {
                mazeNode.run(
                    SKAction.sequence(
                        [SKAction.colorize(with: SKColor.gray, colorBlendFactor: 1, duration: 0.2),
                         SKAction.wait(forDuration: actionDelay),
                         SKAction.colorize(with: SKColor.white, colorBlendFactor: 1, duration: 0),
                         SKAction.colorize(with: SKColor.lightGray, colorBlendFactor: 1, duration: 0.3)]
                    )
                )
            }
        }
    }
}
