/*
 Copyright (C) 2016 Apple Inc. All Rights Reserved.
 See LICENSE.txt for this sample’s licensing information

 Abstract:
 A `UIViewController` subclass that stores references to game-wide input sources and managers.
 */

import UIKit
import SpriteKit

class MapController {
    let scene = MapSKScene(fileNamed: "MapSKScene")!

    init(_ skView: SKView) {
        // Set the scale mode to scale to fit the window.
        scene.scaleMode = .aspectFit

        skView.presentScene(scene)

        // SpriteKit applies additional optimizations to improve rendering performance.
        skView.ignoresSiblingOrder = true
    }

    func start(with map: Map) {
        scene.setup(with: map)
        scene.start()
    }

    func route(from startPoint: Point, to endPoint: Point) {
        scene.route(from: startPoint, to: endPoint)
    }
}
