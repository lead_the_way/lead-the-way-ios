import Foundation

enum Segues: String {
    case ChooseDestinationViewController = "GoToChooseDestinationView"
    case DebugViewController = "GoToDebugView"

    var name: String {
        return self.rawValue
    }
}
