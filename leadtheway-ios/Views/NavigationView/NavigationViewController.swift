import UIKit
import SpriteKit
import Alamofire
import SwiftySound
import AVFoundation

/// The view controller responsible for the main navigation view.
class NavigationViewController: UIViewController {
    // MARK: Properties

    var compassController: Compass?
    var hapticFeedbackEngine: HapticFeedbackEngine?
    var map: Map?
    var mapController: MapController?
    let navigationEngine = NavigationEngine()
    var speechFeedbackEngine: SpeechFeedbackEngine?
    var speechRecognitionEngine: SpeechRecognitionEngine?

    let userTrackingEngine = UserTrackingEngine()

    // TODO: Remove?
    var previousDirection = CardinalDirection.north

    var currentLocationString = "" {
        didSet {
            currentLocationLabel.text = currentLocationString
        }
    }

    var currentInstructionString = "" {
        didSet {
            currentInstructionLabel.text = currentInstructionString
        }
    }

    // MARK: IBOutlet Properties

    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var compassImageView: UIImageView!
    @IBOutlet weak var currentInstructionLabel: UILabel!
    @IBOutlet weak var currentLocationLabel: UILabel!
    @IBOutlet weak var debugButton: UIButton!
    @IBOutlet weak var mapSKView: SKView!

    // MARK: Override Methods

    override func viewWillAppear(_ animated: Bool) {
        debugButton.isHidden = !DEBUG_BTN
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        if DEBUG_SRE {
            speechRecognitionEngine = SpeechRecognitionEngine()
        }

        // Setup controllers.
        compassController = Compass(compassImageView)
        hapticFeedbackEngine = HapticFeedbackEngine()
        mapController = MapController(mapSKView)
        speechFeedbackEngine = SpeechFeedbackEngine()

        // Add notification observers.
        addObservers()

        // Start components.
        startCompass()
        startMap()
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.identifier {
        case Segues.ChooseDestinationViewController.name:
            if let uinvc = segue.destination as? UINavigationController,
                let cdvc = uinvc.topViewController as? ChooseDestinationViewController,
                let data = sender as? OrderedDictionary {
                cdvc.destinations = data
            } else {
                fatalError(
                    "Could not properly cast view controllers and/or data for ChooseDestinationViewController segue."
                )
            }
        default:
            // Nothing special; execute the super `prepare(for:sender:)` method.
            super.prepare(for: segue, sender: sender)
        }
    }

    // MARK: Instance Methods

    /// Add all `NotificationCenter` observers for communication between the `UserTrackingEngine` and this controller.
    func addObservers() {
        // Listen for the current instruction updated notification from the `UserTrackingEngine`.
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.updateCurrentInstruction),
                                               name: Notification.Name(rawValue: "CurrentInstructionUpdated"),
                                               object: nil
        )

        // Listen for the updated user location from the `UserTrackingEngine`.
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.updateCurrentLocation),
                                               name: Notification.Name(rawValue: "CurrentLocationUpdated"),
                                               object: nil
        )

        // Listen for the arrived notification from the `UserTrackingEngine`.
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.updatedCurrentLocationAsArrived),
                                               name: Notification.Name(rawValue: "Arrived"),
                                               object: nil
        )
    }

    /// Update the current instruction label and execute haptic and speech feedback directions.
    ///
    /// - Parameter notification: The notification that triggered the event.
    @objc func updateCurrentInstruction(_ notification: Notification) {
        guard let navigationInstruction = notification.object as? NavigationInstruction else {
            print("ERROR: Could not cast to NavigationInstruction.")
            return
        }

        print("\(navigationInstruction.cardinalDirection) -- \(navigationInstruction.distance)")

        guard let hapticFeedbackEngine = hapticFeedbackEngine, let speechFeedbackEngine = speechFeedbackEngine else {
            fatalError("Either the haptic or speech feedback engine was nil.")
        }

        let relativeDirection = RelativeDirection.convertCardinalDirectionsToRelativeDirection(
            previousDirection, navigationInstruction.cardinalDirection
        )

        previousDirection = navigationInstruction.cardinalDirection

        currentInstructionString = speechFeedbackEngine.getString(
            with: relativeDirection, for: navigationInstruction.toMeters()
        )

        hapticFeedbackEngine.giveDirection(to: relativeDirection, for: navigationInstruction.distance)
        speechFeedbackEngine.giveDirection(to: relativeDirection, for: navigationInstruction.toMeters())
    }

    /// Update the `currentLocationLabel` with the user's updated location.
    ///
    /// - Parameter notification: The notification that triggered the event.
    @objc func updateCurrentLocation(_ notification: Notification) {
        // This guard is needed in case the notifications come out of order.
        // For instance, we may get the "Arrived" notification before the "CurrentLocationUpdated" notification.
        // But once we have arrived we no longer want to
        guard currentLocationString != "Arrived!" else {
            return
        }

        guard let currentLocation = notification.object as? String else {
            print("ERROR: Could not cast to String.")
            return
        }

        currentLocationString = currentLocation
    }

    /// Update the current location when the user has arrived.
    @objc func updatedCurrentLocationAsArrived() {
        currentLocationString = "Arrived!"
        speechFeedbackEngine?.arrived()
    }

    /// Start the user tracking engine given one point and routing to another.
    /// - Note:
    ///
    /// - Parameters:
    ///   - startPoint: The point from where to start.
    ///   - endPoint: The destination point.
    func startNavigation(from startPoint: Point, to endPoint: Point) {
        guard let floor = self.map?.floor else {
            fatalError("Could not load current map's floor.")
        }

        // Always stop just in case.
        userTrackingEngine.stopUserTracking()

        currentLocationString = ""

        // Get the "current" heading for user tracking using the compass.
        if let newDirection = compassController?.getCurrentHeading() {
            self.previousDirection = newDirection
        }

        guard let instructions = navigationEngine.buildNavigationInstructions(from: startPoint, to: endPoint, for: floor, facing: .north) else {
            fatalError("Could not build navigation instructions.")
        }

        let filteredInstructions = filterInstructions(instructions)

        userTrackingEngine.loadInstructions(instructions: filteredInstructions)
        userTrackingEngine.startUserTracking()
    }

    func filterInstructions(_ instructions: [NavigationInstruction]) -> [NavigationInstruction] {
        // Seed filtered instructions with instructions.
        var filteredInstructions = [NavigationInstruction?](instructions)

        // Setup buckets.
        var buckets = [CardinalDirection : Int]()

        // Reduce small turns.
        for i in filteredInstructions.indices {
            // Ensure that both instructions exist and are not out of range.
            guard filteredInstructions[optional: i] != nil else {
                break
            }

            if let distance = filteredInstructions[i]?.distance, let direction = filteredInstructions[i]?.cardinalDirection {
                if distance < 3 {
                    buckets[direction, default: 0] += distance
                    filteredInstructions[i] = nil

                    if let distance = buckets[direction] {
                        if distance >= 4 {
                            filteredInstructions[i] = NavigationInstruction(distance: distance, cardinalDirection: direction)
                            buckets[direction] = 0
                        }
                    }
                    continue
                } else {
                    var index = i - 1
                    for key in buckets.keys {
                        if let value = buckets[key] {
                            if value > 0 {
                                filteredInstructions[index] = NavigationInstruction(distance: value, cardinalDirection: key)
                                index -= 1
                                buckets[key] = 0
                            }
                        }
                    }
                }
            }
        }

        // Empty any remaining values in the buckets.
        for key in buckets.keys {
            if let value = buckets[key] {
                if value > 0 {
                    filteredInstructions.append(NavigationInstruction(distance: value, cardinalDirection: key))
                    buckets[key] = 0
                }
            }
        }

        // Remove values less than 2 units (0.5 meters).
        filteredInstructions = filteredInstructions.filter {
            if let instruction = $0, instruction.distance > 2 {
                return true
            }

            return false
        }

        // Combine consecutive instructions that are in the same direction.
        for i in filteredInstructions.indices {
            guard filteredInstructions[optional: i + 1] != nil else {
                break
            }

            if filteredInstructions[i]?.cardinalDirection == filteredInstructions[i + 1]?.cardinalDirection {
                if let addingDistance = filteredInstructions[i]?.distance {
                    filteredInstructions[i + 1]?.distance += addingDistance
                }
                filteredInstructions[i] = nil
            }
        }

        // Remove nil values.
        let finalFilteredInstructions = filteredInstructions.compactMap { $0 }

        print("***** Filtered Instructions (Final) *****")
        finalFilteredInstructions.forEach {
            print("\($0.cardinalDirection) -- \($0.distance)")
        }
        print("*****************************************")

        return finalFilteredInstructions
    }

    /// Start the Compass.
    ///
    /// If the compass cannot be started then halt the application.
    func startCompass() {
        guard let compassController = compassController else {
            fatalError("Could not start compass because compassController is nil.")
        }

        compassController.start()
    }

    /// Use the `mapController` to route from the current location to the supplied location.
    ///
    /// - Parameter destination: The intended destination in which to route.
    func route(from start: String = "Exit_1", to destination: String) {
        // TODO: Remove the static start location.
        guard let startDestination = map?.floor.destinations?.first(where: {$0.description == start}) else {
            print("Could not find start location with name \(start)")
            return
        }

        guard let endDestination = map?.floor.destinations?.first(where: {$0.description == destination}) else {
            print("Could not find destination with name \(destination)")
            return
        }

        let startPoint = Point(id: 0, x: startDestination.x, y: startDestination.y, obstructionID: startDestination.id)
        let endPoint = Point(id: 0, x: endDestination.x, y: endDestination.y, obstructionID: endDestination.id)

        mapController?.route(from: startPoint, to: endPoint)

        startNavigation(from: startPoint, to: endPoint)
    }

    /// Fetch the map data and feed it to the `NavigationEngine` and `MapController`.
    func startMap() {
        // If in DEBUG mode, then skip this method and use `startBackupMap()` instead.
        if DEBUG_API {
            startBackupMap()
            return
        }

        // Start the activity indicator.
        activityIndicator.startAnimating()

        // Hit the API endpoint with the test floor (HEC Floor 1).
        let apiUrlWithEndpoint = Config.apiURL + "/" +
            Config.floor + "/" +
            String(1)

        request(apiUrlWithEndpoint).responseJSON { response in
            guard let data = response.data else {
                print("ERROR: Could not get data from JSON response.")
                return
            }

            // Once this network response block is finished, stop the activity indicator.
            defer {
                self.activityIndicator.stopAnimating()
            }

            // Try decoding the JSON data into Floor object.
            do {
                let jsonDecoder = JSONDecoder()
                let floor = try jsonDecoder.decode(Floor.self, from: data)

                self.navigationEngine.addMap(with: floor)

                self.map = self.navigationEngine.getMap()

                if let map = self.map, let mapController = self.mapController {
                    mapController.start(with: map)
                }
            } catch {
                print("ERROR: Could not decode JSON response data into a Floor object.")
            }
        }
    }

    /// This is a backup `startMap()` method in case networking goes down.
    ///
    /// This should work the same way as the regular method, but with local data instead of a network call.
    func startBackupMap() {
        // Start the activity indicator.
        activityIndicator.startAnimating()

        guard let floor = getTestFloor() else {
            fatalError("Could not generate local test floor.")
        }

        self.navigationEngine.addMap(with: floor)

        self.map = self.navigationEngine.getMap()

        if let map = self.map, let mapController = self.mapController {
            mapController.start(with: map)
        }

        // Stop the activity indicator.
        self.activityIndicator.stopAnimating()
    }

    /// Create an `OrderedDictionary` given an array of `Destination` objects.
    ///
    /// - Parameter destinations: An array of `Destination` objects to transform.
    /// - Returns: An `OrderedDictionary` generated from the supplied array of `Destination` objects.
    func buildOrderedDictionary(from destinations: [Destination]) -> OrderedDictionary {
        var destinationsOrderedDictionary = OrderedDictionary()

        destinations.forEach { destination in
            destinationsOrderedDictionary.addPair(
                destination.type.rawValue, destination.description ?? "MissingNo."
            )
        }

        return destinationsOrderedDictionary
    }

    // MARK: IBAction Methods

    /// Navigate to the debug view.
    ///
    /// - Parameter sender: The button that initiated the action.
    @IBAction func debugButtonPressed(_ sender: UIButton) {
        performSegue(withIdentifier: Segues.DebugViewController.name, sender: nil)
    }

    /// Navigate to the view that allows the user to choose a destination.
    ///
    /// - Parameter sender: The button that initiated the action.
    @IBAction func chooseDestinationButtonPressed(_ sender: UIButton) {
        let destinations = buildOrderedDictionary(from: map?.floor.destinations ?? [])

        performSegue(withIdentifier: Segues.ChooseDestinationViewController.name, sender: destinations)
    }

    @IBAction func zoom(_ sender: UIPinchGestureRecognizer) {
        guard let scene = mapController?.scene else {
            fatalError("ERROR: Zoom problem.")
        }

        let anchorPoint = scene.convertPoint(fromView: sender.location(in: sender.view))
        let anchorPointInMySkNode = scene.cameraNode.convert(anchorPoint, from: scene)

        scene.cameraNode.setScale(scene.cameraNode.xScale * sender.scale)

        let mySkNodeAnchorPointInScene = scene.convert(anchorPointInMySkNode, from: scene.cameraNode)

        let translationOfAnchorInScene = (x: anchorPoint.x - mySkNodeAnchorPointInScene.x,
                                          y: anchorPoint.y - mySkNodeAnchorPointInScene.y)

        scene.cameraNode.position = CGPoint(x: scene.cameraNode.position.x + translationOfAnchorInScene.x,
                                            y: scene.cameraNode.position.y + translationOfAnchorInScene.y)

        sender.scale = 1.0
    }

    @IBAction func compassPressed(_ sender: UITapGestureRecognizer) {
        if DEBUG_SRE {
            speechFeedbackEngine?.announceSpeechRecognition()

            if speechRecognitionEngine?.isRunning() ?? false {
                speechRecognitionEngine?.stopSpeechRecording()
            } else {
                try? speechRecognitionEngine?.startSpeechRecognition { startLocation, endLocation in
                    if let startLocation = startLocation, let endLocation = endLocation {
                        self.route(from: startLocation, to: endLocation)
                        return
                    }

                    if let endLocation = endLocation {
                        self.route(to: endLocation)
                        return
                    }

                    print("ERROR: Could not get start and end locations.")
                }
            }
        }
    }
}
