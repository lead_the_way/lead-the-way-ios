import UIKit

class PickerController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {
    // MARK: Properties

    var data = [Destination]()

    // MARK: Initializers

    /// Initialize and set the data source.
    ///
    /// - Note: This convenience initializer will not remove the default initializers.
    ///
    /// - Parameter data: The data source that will default to an empty array.
    convenience init(with data: [Destination] = []) {
        self.init()
        self.data = data
    }

    // MARK: Delegate Methods

    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        // Always return 1 because we are only supporting one "component" (column).
        return 1
    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return data.count
    }

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return data[row].description ?? "MissingNo."
    }
}
