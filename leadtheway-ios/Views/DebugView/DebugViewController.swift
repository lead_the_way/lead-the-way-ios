import UIKit
import Alamofire

class DebugViewController: UIViewController {
    // MARK: Properties

    var startPoints = [String]()
    var endPoints = [String]()
    let startPointPickerController = PickerController()
    let endPointPickerController = PickerController()
    let navigationInstructionSet = NavigationInstructionSet()

    let navigationEngine = NavigationEngine()
    let userTrackingEngine = UserTrackingEngine()

    var testFloor: Floor? = nil {
        didSet {
            // Get the destinations from the new test floor.
            let destinations = testFloor?.destinations ?? []

            // Clear the current picker options.
            startPointPickerController.data = []
            endPointPickerController.data = []

            // Set new picker options.
            destinations.forEach { destination in
                startPointPickerController.data.append(destination)
                endPointPickerController.data.append(destination)
            }

            // Refresh the picker text options.
            startPointPicker.reloadAllComponents()
            endPointPicker.reloadAllComponents()
        }
    }

    // MARK: IBOutlet Properties

    @IBOutlet weak var startPointPicker: UIPickerView!
    @IBOutlet weak var endPointPicker: UIPickerView!
    @IBOutlet weak var currentLocationLabel: UILabel!
    @IBOutlet weak var navigationInstructionsLabel: UILabel!
    @IBOutlet weak var routeButton: UIButton!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!

    // MARK: Override Methods

    override func viewDidLoad() {
        super.viewDidLoad()

        self.startPointPicker.delegate = startPointPickerController
        self.endPointPicker.delegate = endPointPickerController

        updateNavigationInstructions(for: navigationInstructionsLabel, with: navigationInstructionSet)

        // Listen for the current instruction updated notification from the `UserTrackingEngine`.
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.nextInstruction),
                                               name: Notification.Name(rawValue: "CurrentInstructionUpdated"),
                                               object: nil
        )

        // Listen for the updated user location.
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.updateCurrentLocationLabel),
                                               name: Notification.Name(rawValue: "CurrentLocationUpdated"),
                                               object: nil
        )

        // Listen for the arrived notification.
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.updatedCurrentLocationLabelWithArrived),
                                               name: Notification.Name(rawValue: "Arrived"),
                                               object: nil
        )
    }

    // MARK: Instance Methods

    /// Update the `currentLocationLabel` with the user's updated location.
    @objc func updateCurrentLocationLabel(_ notification: Notification) {
        if currentLocationLabel.text == "Arrived!" {
            return
        }

        if let currentLocation = notification.object as? String {
            currentLocationLabel.text = currentLocation
        } else {
            currentLocationLabel.text = "TEST"
        }
    }

    @objc func updatedCurrentLocationLabelWithArrived() {
        currentLocationLabel.text = "Arrived!"
    }

    /// Update the display of nagivation instructions.
    ///
    /// - Parameters:
    ///   - label: The UILabel to be updated.
    ///   - set: The NavigationInstructionSet from which to get the navigation instructions.
    func updateNavigationInstructions(for label: UILabel, with set: NavigationInstructionSet) {
        var newLabel = ""

        for i in set.navigationInstructions.indices {
            let instruction = set.navigationInstructions[i]

            if i == set.currentIndex {
                newLabel.append(set.getHighlightNavigationInsturctionDescription(instruction))
            } else {
                newLabel.append(set.getNavigationInstructionDescription(instruction))
            }

            newLabel.append("\n")
        }

        label.text = newLabel
    }

    /// Get the current selected value of a UIPickerView.
    ///
    /// - Parameters:
    ///   - picker: The picker from which to get the value.
    ///   - data: The data source for the picker.
    /// - Returns: A String that represents the current selected value.
    func getSelectedValueForPicker(_ picker: UIPickerView, with data: [Destination]) -> Destination {
        return data[picker.selectedRow(inComponent: 0)]
    }

    /// Go to the next instruction in the set.
    @objc func nextInstruction() {
        let result = navigationInstructionSet.goToNextInstruction()

        if result == false {
            print("Could no go to next instruction.")
        }

        updateNavigationInstructions(for: navigationInstructionsLabel, with: navigationInstructionSet)
    }

    // MARK: IBAction Methods

    /// This is currently a stub.
    ///
    /// - Parameter sender: The UIButton that initiated the action.
    @IBAction func routeButtonPressed(_ sender: UIButton) {
        navigationInstructionSet.resetCurrentIndex()

        let startLocation = getSelectedValueForPicker(startPointPicker, with: startPointPickerController.data)
        let endLocation = getSelectedValueForPicker(endPointPicker, with: endPointPickerController.data)

        let firstPoint = Point(id: startLocation.id, x: startLocation.x, y: startLocation.y, obstructionID: 0)
        let secondPoint = Point(id: endLocation.id, x: endLocation.x, y: endLocation.y, obstructionID: 0)

        guard let testFloor = testFloor else {
            print("Test floor is not set.")
            return
        }

        let instructions = navigationEngine.buildNavigationInstructions(from: firstPoint, to: secondPoint,
                                                                        for: testFloor, facing: .north)

        if let instructions = instructions {
            navigationInstructionSet.navigationInstructions = instructions
            updateNavigationInstructions(for: navigationInstructionsLabel, with: navigationInstructionSet)

            userTrackingEngine.loadInstructions(instructions: instructions)
            userTrackingEngine.startUserTracking()

            for instruction in instructions {
                print(
                    navigationInstructionSet.getNavigationInstructionDescription(instruction) +
                        " " + "(\(instruction.cardinalDirection))"
                )
            }
        }

        currentLocationLabel.text = ""
    }

    /// Go the the next instruction. This is a test button.
    ///
    /// - Parameter sender: The UIButton that initiated the action.
    @IBAction func nextButtonPressed(_ sender: UIButton) {
        nextInstruction()
    }

    /// Dismiss the debug view.
    @IBAction func doneButtonPressed(_ sender: UIButton) {
        tabBarController?.dismiss(animated: true)
    }

    /// Sets up testing data when the **Super Secret Test Button** is pressed.
    ///
    /// - Note: This uses the `https://dev.leadtheway.app/test` endpoint.
    ///
    /// - Parameter sender: The UIButton that initiated the action.
    @IBAction func superSecretTestButtonPressed(_ sender: UIButton) {
        // Start the activity indicator.
        activityIndicator.startAnimating()

        let apiUrlWithEndpoint = Config.apiURL + "/test"

        request(apiUrlWithEndpoint).responseJSON { response in
            // Once this network response block is finished, stop the activity indicator.
            defer {
                self.activityIndicator.stopAnimating()
            }

            if let data = response.data {
                let jsonDecoder = JSONDecoder()
                do {
                    self.testFloor = try jsonDecoder.decode(Floor.self, from: data)

                    if let testFloor = self.testFloor {
                        self.navigationEngine.addMap(with: testFloor)
                        print("Test floor successfully loaded from API.")
                    }
                } catch {
                    print("Could not decode API data into floor.")
                }
            }
        }
    }
}
