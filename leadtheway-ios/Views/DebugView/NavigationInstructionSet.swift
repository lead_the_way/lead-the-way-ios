import Foundation

class NavigationInstructionSet {
    // MARK: Properties

    var navigationInstructions: [NavigationInstruction]
    var currentIndex: Int

    // MARK: Initializers

    init() {
        navigationInstructions = []
        currentIndex = 0
    }

    // MARK: Methods

    /// Resets the current index to 0.
    func resetCurrentIndex() {
        currentIndex = 0
    }

    /// Increment the current index if possible.
    ///
    /// - Returns: True if the value was incremented; false otherwise.
    func goToNextInstruction() -> Bool {
        let newIndex = currentIndex + 1

        if newIndex < navigationInstructions.count {
            currentIndex = newIndex
            return true
        } else {
            return false
        }
    }

    /// This **safely** gets the current navigation instruction by first ensuring that the index exists.
    ///
    /// - Returns: The current nagivation instruction if it exists; otherwise nil.
    func getCurrentNavigationInstruction() -> NavigationInstruction? {
        return navigationInstructions.indices.contains(currentIndex) ? navigationInstructions[currentIndex] : nil
    }

    /// Determines if the supplied NavigationInstruction is the current one.
    ///
    /// - Parameter instruction: The instruction in question.
    /// - Returns: True if the supplied NavigationInstruction is the current one; false otherwise.
    func isCurrentNavigationInstruction(_ instruction: NavigationInstruction) -> Bool {
        if let index = navigationInstructions.index(of: instruction) {
            return index == currentIndex
        }

        return false
    }

    /// This is a test method for populating the data.
    ///
    /// - Returns: Some test data in the form of NavigationInstruction objects.
    static func getNavigationInstructions() -> [NavigationInstruction] {
        let ni1 = NavigationInstruction(distance: 1, cardinalDirection: .north)
        let ni2 = NavigationInstruction(distance: 8, cardinalDirection: .east)
        let ni3 = NavigationInstruction(distance: 14, cardinalDirection: .south)
        let ni4 = NavigationInstruction(distance: 3, cardinalDirection: .west)

        return [ni1, ni2, ni3, ni4]
    }

    /// Gets a String for a relative direction.
    ///
    /// - Parameter direction: The direction to "convert."
    /// - Returns: The String value for the supplied direction.
    func getRelativeDirectionString(_ direction: CardinalDirection) -> String {
        switch direction {
        case .north:
            return "north"
        case .west:
            return "west"
        case .south:
            return "south"
        case .east:
            return "east"
        }
    }

    /// Converts "units" into multiples of a quarter meter.
    ///
    /// A "unit" is 0.28 meters.
    ///
    ///     1 unit  ->  .25 meters
    ///     2 units ->  .50 meters
    ///     3 units ->  .75 meters
    ///     4 units -> 1.00 meters
    ///     5 units -> 1.25 meters
    ///
    /// - Parameter distance: The distance to convert.
    /// - Returns: The converted value in meters.
    func convertUnitToMeters(_ distance: Int) -> Double {
        return Double(distance) / 4.0
    }

    /// Get a readable description for a navigation instruction.
    ///
    /// - Example: Move left 2 meters
    ///
    /// - Parameter instruction: The instruction from which to build the description.
    /// - Returns: A String representation of the NavigationInstruction.
    func getNavigationInstructionDescription(_ instruction: NavigationInstruction) -> String {
        let direction = getRelativeDirectionString(instruction.cardinalDirection)
        let distance = convertUnitToMeters(instruction.distance)
        let unit = "meter"
        let plural = (distance == 1) ? "" : "s"

        return "Move \(direction) \(distance) \(unit)\(plural)"
    }

    /// A special description that is outline with carets.
    ///
    /// - Example: > Move left 2 meters <
    ///
    /// - Parameter instruction: The instruction from which to build the description.
    /// - Returns: A String representation of the NavigationItem highlighted with carets.
    func getHighlightNavigationInsturctionDescription(_ instruction: NavigationInstruction) -> String {
        return "> \(getNavigationInstructionDescription(instruction)) <"
    }
}
