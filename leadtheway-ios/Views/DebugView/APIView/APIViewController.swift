import UIKit
import Alamofire

/// Controller reponsible for the API test view.
class APIViewController: UIViewController {
    // MARK: Properties

    let apiUrl = Config.apiURL
    let buildingNumber = "0"

    // MARK: IBOutlet Properties

    @IBOutlet weak var postTextField: UITextField!
    @IBOutlet weak var deleteTextField: UITextField!
    @IBOutlet weak var responseLabel: UILabel!

    // MARK: Instance Methods

    /// Dismiss the keyboard from displaying.
    func dismissKeyboard() {
        postTextField.resignFirstResponder()
        deleteTextField.resignFirstResponder()
    }

    // MARK: IBAction Methods

    @IBAction func getButtonPressed(_ sender: Any) {
        let apiUrlWithEndpoint = apiUrl + "/building/" + buildingNumber
        print(apiUrl)

        request(apiUrlWithEndpoint).responseJSON { response in
            if let data = response.data, let responseText = String(data: data, encoding: .utf8) {
                // Print the response as text to the console for debugging reasons.
                print("Data: \(responseText)")

                // Setup a floor object to hold decoded Floor data.
                var building: Building?

                // Try decoding the JSON data into Floor object.
                do {
                    let jsonDecoder = JSONDecoder()
                    building = try jsonDecoder.decode(Building.self, from: data)
                } catch {
                    print("ERROR: Could not decode JSON response data.")
                }

                // Assign some descriptors to ensure the Building object is working correctly.
                let formattedResponse = "BuildingID: \(String(describing: building?.id.description))\n"
                    + "Name: \(String(describing: building?.name?.description))"

                // Set the responseLabel to show the formatted response.
                self.responseLabel.text = formattedResponse

                self.dismissKeyboard()
            }
        }
    }

    @IBAction func postButtonPressed(_ sender: Any) {
        let apiUrlWithEndpoint = apiUrl + "/todos"

        let parameters: Parameters = [
            "title": postTextField.text ?? ""
        ]

        request(apiUrlWithEndpoint, method: .post, parameters: parameters).responseJSON { response in
            if let data = response.data, let responseText = String(data: data, encoding: .utf8) {
                print("Data: \(responseText)")

                self.responseLabel.text = responseText

                // Clear the text field.
                self.postTextField.text = ""

                self.dismissKeyboard()
            }
        }
    }

    @IBAction func deleteButtonPressed(_ sender: Any) {
        let id = deleteTextField.text ?? "0"

        let apiUrlWithEndpointAndId = apiUrl + "/todos/" + id

        request(apiUrlWithEndpointAndId, method: .delete).responseJSON { response in
            if let data = response.data, let responseText = String(data: data, encoding: .utf8) {
                print("Data: \(responseText)")

                // No response is actually a sucessful response.
                self.responseLabel.text = (responseText != "") ? responseText : Strings.successfullyDeleted

                // Clear the text field.
                self.deleteTextField.text = ""

                self.dismissKeyboard()
            }
        }
    }
}
