import Foundation
import Alamofire

/// Use the API to add a `Building` to the database.
///
/// - Remark: `POST {api_url}/building`
///
/// - Parameter building: The Building to be added to the database.
/// - Returns: If successful, the new building's ID in the database; otherwise nil.
func addBuilding(_ building: Building) -> Int? {
    let apiUrlWithEndpoint = Config.apiURL + "/" +
        Config.building

    // Print the full API endpoint to the console for debugging.
    if Config.isDevelopment() {
        print(apiUrlWithEndpoint)
    }

    // Setup an Int to hold the new Building's assigned ID.
    var newBuildingID: Int?

    guard let name = building.name, let longitude = building.longitude, let latitude = building.latitude else {
        print("ERROR: Could not derive Building parameter data from supplied Buliding object.")
        return nil
    }

    let parameters: Parameters = [
        "name": name,
        "longitude": longitude,
        "latitude": latitude
    ]

    request(apiUrlWithEndpoint, method: .post, parameters: parameters).responseJSON { response in
        if let data = response.data, let responseText = String(data: data, encoding: .utf8) {
            // Print the response to the console for debugging.
            if Config.isDevelopment() {
                print("Data: \(responseText)")
            }

            // Try decoding the JSON data into Building object.
            do {
                let jsonDecoder = JSONDecoder()
                let building = try jsonDecoder.decode(Building.self, from: data)

                // If the Building was successfully created then prepare to return the new Building ID.
                newBuildingID = building.id
            } catch {
                print("ERROR: Could not decode JSON response data into a Building array.")
            }
        }
    }

    return newBuildingID
}

/// Utilizes the API to get a set of buildings within a certain range of a latitude and longitude point.
///
/// - Remark: `GET {api_url}/building/within/{latitude: Double}/{longitude: Double}/{range: Double}`
///
/// - Parameters:
///   - latitude: The latitude of the desired search location.
///   - longitude: The longitude of the desired search location.
///   - range: The range (radius) of the area to search.
/// - Returns: An array of buildings that meet the criteria; if no buildings are found this returns nil.
func getBuildingsWithin(latitude: Double, longitude: Double, withRange range: Double) -> [Building]? {
    let apiUrlWithEndpoint = Config.apiURL + "/" +
        Config.building + "/" +
        Config.within + "/" +
        String(latitude) + "/" +
        String(longitude) + "/" +
        String(range)

    // Print the full API endpoint to the console for debugging.
    if Config.isDevelopment() {
        print(apiUrlWithEndpoint)
    }

    // Setup a Building array to hold decoded Building array data.
    var buildings: [Building]?

    request(apiUrlWithEndpoint).responseJSON { response in
        if let data = response.data, let responseText = String(data: data, encoding: .utf8) {
            // Print the response to the console for debugging.
            if Config.isDevelopment() {
                print("Data: \(responseText)")
            }

            // Try decoding the JSON data into Floor object.
            do {
                let jsonDecoder = JSONDecoder()
                buildings = try jsonDecoder.decode([Building].self, from: data)
            } catch {
                print("ERROR: Could not decode JSON response data into a Building array.")
            }
        }
    }

    return buildings
}

/// Uses the API to get a `Building` with its ID in the database.
///
/// - Remark: `GET {api_url}/building/{building_id: Int}`
///
/// - Parameter id: The `Building`'s ID in the database.
/// - Returns: The `Building` object if found; otherwise nil.
func getBuilding(_ id: Int) -> Building? {
    let apiUrlWithEndpoint = Config.apiURL + "/" +
        Config.building + "/" +
        String(id)

    // Print the full API endpoint to the console for debugging.
    if Config.isDevelopment() {
        print(apiUrlWithEndpoint)
    }

    // Setup a Building object to hold decoded Building data.
    var building: Building?

    request(apiUrlWithEndpoint).responseJSON { response in
        if let data = response.data, let responseText = String(data: data, encoding: .utf8) {
            // Print the response to the console for debugging.
            if Config.isDevelopment() {
                print("Data: \(responseText)")
            }

            // Try decoding the JSON data into Floor object.
            do {
                let jsonDecoder = JSONDecoder()
                building = try jsonDecoder.decode(Building.self, from: data)
            } catch {
                print("ERROR: Could not decode JSON response data into a Building object.")
            }
        }
    }

    return building
}

/// Uses the API to get a `Floor` with its ID in the database.
///
/// - Remark: `GET {api_url}/floor/{floor_id: Int}`
///
/// - Parameter id: The `Floor`'s ID in the database.
/// - Returns: The `Floor` object if found; otherwise nil.
func getFloor(_ id: Int) -> Floor? {
    let apiUrlWithEndpoint = Config.apiURL + "/" +
        Config.floor + "/" +
        String(id)

    // Print the full API endpoint to the console for debugging.
    if Config.isDevelopment() {
        print(apiUrlWithEndpoint)
    }

    // Setup a Floor object to hold decoded Floor data.
    var floor: Floor?

    request(apiUrlWithEndpoint).responseJSON { response in
        if let data = response.data {
            // Try decoding the JSON data into Floor object.
            do {
                let jsonDecoder = JSONDecoder()
                floor = try jsonDecoder.decode(Floor.self, from: data)
            } catch {
                print("ERROR: Could not decode JSON response data into a Floor object.")
            }
        }
    }

    return floor
}
