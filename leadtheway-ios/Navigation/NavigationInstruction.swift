import Foundation

public struct NavigationInstruction: Equatable {
    var distance: Int
    let cardinalDirection: CardinalDirection

    /// Converts "units" into multiples of a quarter meter.
    ///
    /// A "unit" is 0.28 meters.
    ///
    ///     1 unit  ->  .25 meters
    ///     2 units ->  .50 meters
    ///     3 units ->  .75 meters
    ///     4 units -> 1.00 meters
    ///     5 units -> 1.25 meters
    ///
    /// - Parameter distance: The distance to convert.
    /// - Returns: The converted value in meters.
    func convertUnitToMeters(_ distance: Int) -> Double {
        return Double(distance) / 4.0
    }

    func toMeters() -> Double {
        return convertUnitToMeters(self.distance)
    }
}
