import Foundation

public class NavigationEngine {
    private var maps = [Map]()

    func getMap() -> Map {
        return maps[0]
    }

    func addMap(with floor: Floor) {
        let newMap = Map(width: floor.width, height: floor.depth, for: floor)

        maps.append(newMap)
    }

    private func getPath(from firstPoint: Point, to secondPoint: Point, for floor: Floor) -> [Point]? {
        let wantedMap = maps.filter { map in
            return map.floor.id == floor.id
            }.first

        return wantedMap?.getPath(from: firstPoint, to: secondPoint)
    }

    func buildNavigationInstructions(from firstPoint: Point, to secondPoint: Point, for floor: Floor,
                                     facing currentDirection: CardinalDirection) -> [NavigationInstruction]? {
        guard let path = getPath(from: firstPoint, to: secondPoint, for: floor) else {
            return nil
        }

        var currentCardinalDirection = currentDirection
        var navigationInstructions = [NavigationInstruction]()
        let intersections = getIntersection(for: path)
        var current = intersections[0]

        for index in 1..<intersections.count {
            let distance = getDistance(firstPoint: current, secondPoint: intersections[index])
            let cardinalDirection = getCardinalDirection(firstPoint: current, secondPoint: intersections[index],
                                                         facing: currentCardinalDirection)

            let navigationInstruction = NavigationInstruction(distance: distance, cardinalDirection: cardinalDirection)
            navigationInstructions.append(navigationInstruction)

            currentCardinalDirection = cardinalDirection
            current = intersections[index]
        }

        return navigationInstructions
    }

    private func getMovementDerivates(_ firstPoint: Point, _ secondPoint: Point) -> (MovementResult, MovementResult) {
        let changeX = secondPoint.x - firstPoint.x
        let changeY = secondPoint.y - firstPoint.y
        var xMove: MovementResult
        var yMove: MovementResult

        if changeX > 0 {
            xMove = .positive
        } else if changeX < 0 {
            xMove = .negative
        } else {
            xMove = .zero
        }

        if changeY > 0 {
            yMove = .positive
        } else if changeY < 0 {
            yMove = .negative
        } else {
            yMove = .zero
        }

        return (xMove, yMove)
    }

    private func getCardinalDirection(firstPoint: Point, secondPoint: Point, facing: CardinalDirection)
        -> CardinalDirection {
            var direction: CardinalDirection
            let (xMove, yMove) = getMovementDerivates(firstPoint, secondPoint)

            switch(xMove, yMove) {
            case (.positive, .zero):
                direction = .east
            case (.negative, .zero):
                direction = .west
            case (.zero, .positive):
                direction = .north
            case (.zero, .negative):
                direction = .south
            default:
                direction = .north
            }

            return direction
    }

    private func getDistance(firstPoint: Point, secondPoint: Point) -> Int {
        return firstPoint.x == secondPoint.x ? abs(firstPoint.y - secondPoint.y) : abs(firstPoint.x - secondPoint.x)
    }

    private func getIntersection(for points: [Point]) -> [Point] {
        var current = points[0]
        var intersections = [Point]()
        intersections.append(current)

        for i in 1..<points.count {
            if points[i].x != current.x && points[i].y != current.y {
                intersections.append(points[i - 1])
                current = points[i - 1]
            }
        }

        intersections.append(points[points.count-1])

        return intersections
    }
}
