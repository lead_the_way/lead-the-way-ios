import Foundation

public enum MovementResult {
    case positive
    case negative
    case zero
}
