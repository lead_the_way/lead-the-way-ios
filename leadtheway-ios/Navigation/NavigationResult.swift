import Foundation

public enum NavigationResult {
    case success
    case userLost
    case userTrackingError
    case pathNotFound
}
