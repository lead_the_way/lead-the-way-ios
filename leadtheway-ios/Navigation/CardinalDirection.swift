import Foundation

public enum CardinalDirection {
    case north
    case east
    case south
    case west

    func index() -> Int {
        switch self {
        case .north:
            return 0
        case .east:
            return 1
        case .south:
            return 2
        case .west:
            return 3
        }
    }
}
