import Foundation
import GameplayKit

class MapBuilder {
    /**
     An enum for the cardinal directions. This enables you to randomly
     generate a direction with a random number generator.
     */
    enum Direction: Int {
        case left = 0, down, right, up

        static func random() -> Direction {
            let randomInt = Int.random(in: 0...3)

            if let direction = Direction(rawValue: randomInt) {
                return direction
            } else {
                fatalError("Unable to find direction for Direction enum \(randomInt).")
            }
        }

        // The offset value for the x-axis associated with a direction.
        var dx: Int {
            switch self {
            case .up, .down: return 0
            case .left:      return -2
            case .right:     return 2
            }
        }

        // The offset value for the y-axis associated with a direction.
        var dy: Int {
            switch self {
            case .left, .right: return 0
            case .up:           return 2
            case .down:         return -2
            }
        }
    }

    // MARK: Properties

    /// A reference to the map that the map builder is building for.
    let map: Map

    /// Holds graph nodes designated as walls.
    var wallNodes = [GKGridGraphNode]()

    /// Used as a stack to search the map during during map generation.
    var searchStack = [GKGridGraphNode]()

    /// Used to keep track of visited nodes during map generation.
    var visitedNodes = [GKGridGraphNode]()

    /// Returns every potential wall in the map graph to the walls array.
    func getPotentialWalls(_ obstructions: [Obstruction]) -> [GKGridGraphNode] {
        // Grab the graph nodes from the map graph.
        var obstructionNodes = [GKGridGraphNode]()

        /*
         This grabs all the obstructions, grabs all the points; adds all those points to what is called an
         obstruction point vector, allowing us to get all intermediary nodes. All the intermediary nodes between
         two points in the obstruction vector will be added to the obstruction nodes, allowing us to remove all
         the nodes associated to an obstruction.
         */
        for obstruction in obstructions {
            if let points = obstruction.points {
                var obstructionPointVector = points.map {
                    vector_int2(Int32($0.x), Int32($0.y))
                }

                /*
                 We are choosing to go from 0 to the count of obstructionPointVectors - 2 if not we will be throwing
                 an overflow exception, attempting to access a node not accounted for by the array.
                 */
                for index in (0...obstructionPointVector.count - 2) {
                    guard let firstNode = map.graph.node(atGridPosition: obstructionPointVector[index]),
                        let secondNode = map.graph.node(atGridPosition: obstructionPointVector[index + 1]) else {
                            fatalError("Either first node or second node resulted in a nil value.")
                    }

                    let path = map.graph.findPath(from: firstNode, to: secondNode)

                    for node in path {
                        if let node = node as? GKGridGraphNode {
                            obstructionNodes.append(node)
                        }
                    }
                }
            }
        }

        return obstructionNodes
    }

    // MARK: Initialization

    init(map: Map) {
        self.map = map
    }

    // MARK: Methods

    /**
     Returns an array of map graph nodes representing walls in the map.
     These nodes are to be removed from the pathfinding graph, since walls
     are impassible.

     This map generation algorithm uses a depth-first search (DFS).
     It uses a stack to track its progress through the map, and an array
     to check how much of the map it has visited. It works like this:
     the starting node is added to the stack and array. The algorithm
     selects a node neighboring the top node of the stack (the starting
     node, in this case). It then removes the wall separating those two
     nodes, and adds the neighboring node to the stack and array. This
     process continues until the top node of the stack has no unvisited
     neighbors. When what happens, the algorithm removes nodes from the
     stack until the top node has an unvisited neighbor, and the process
     continues. Eventually the entire map will have been visited, the
     stack will be empty, and the map is created.

     Instead of removing walls directly, this method keeps track of
     which walls need to be removed, and returns those nodes.
     */
    func mapWallsForRemoval(startNode: GKGridGraphNode, obstructions: [Obstruction]) -> [GKGridGraphNode] {
        // First, add all of the potential walls to the array of walls.
        wallNodes += getPotentialWalls(obstructions)

        // Initialize both the stack and array with the starting map graph node.
        //        searchStack.append(startNode)
        //        visitedNodes.append(startNode)
        //
        //        // Until the stack is empty, process the map graph.
        //        while let topNode = searchStack.last {
        //            /*
        //             First, check if the top node of the stack has any unvisited
        //             neighbors. If so, select a random unvisited neighbor to visit.
        //             Otherwise, remove the top node.
        //             */
        //            guard hasUnvisitedNeighborNode(topNode) else {
        //                // Remove the top node.
        //                searchStack.removeLast()
        //                // Skip to the next iteration of the while loop.
        //                continue
        //            }
        //
        //            /*
        //             Check random neighboring directions until a neighboring node
        //             is found. Then visit that node.
        //             */
        //            exploreUnvisitedNodes: while true {
        //                // Generate a random direction.
        //                let randomDirection = Direction.random()
        //
        //                /*
        //                 If a direction should be explored by the algorithm, explore
        //                 the node in that direction and exit the while loop.
        //                 */
        //                if shouldExploreInDirectionFromNode(topNode, inDirection: randomDirection) {
        //                    exploreNodeInDirectionFromNode(topNode, inDirection: randomDirection)
        //                    break exploreUnvisitedNodes
        //                }
        //            }
        //        }

        // Return a set of walls that can be removed to form a map.
        return wallNodes
    }

    /**
     Tests whether a node in a direction from a given node is unvisited. If
     so, it is explorable by the map generation algorithm.
     */
    func shouldExploreInDirectionFromNode(_ node: GKGridGraphNode, inDirection direction: Direction) -> Bool {
        // Get the direction of the offset.
        let dx = direction.dx
        let dy = direction.dy

        // Get the location of the current node.
        let x = node.gridPosition.x
        let y = node.gridPosition.y

        // Return whether the node is unvisited or not.
        return nodeIsUnvisitedAtCoordinates(x: x.advanced(by: dx), y: y.advanced(by: dy))
    }

    /**
     Explores a direction in the map generation algorithm, removing the wall
     between the given node and a node in the given direction.
     */
    func exploreNodeInDirectionFromNode(_ node: GKGridGraphNode, inDirection direction: Direction) {
        // Get the direction of the offset.
        let dx = Int32(direction.dx)
        let dy = Int32(direction.dy)

        // Get the location of the current node.
        let x = node.gridPosition.x
        let y = node.gridPosition.y

        // Get the location of node in the given direction.
        let nodeInDirectionPosition = int2(x + Int32(dx), y + Int32(dy))
        if let nodeInDirection = map.graph.node(atGridPosition: nodeInDirectionPosition) {

            // Add the node in the direction to the stack, and mark it as visited.
            searchStack.append(nodeInDirection)
            visitedNodes.append(nodeInDirection)
        }
    }

    /// Checks if the given map graph node has any unvisited neighbor nodes.
    func hasUnvisitedNeighborNode(_ currentNode: GKGridGraphNode) -> Bool {
        // Grab the position of the given map graph node.
        let x = currentNode.gridPosition.x
        let y = currentNode.gridPosition.y

        // Check whether the left, right, top, or bottom nodes are unvisited.
        let leftNodeIsUnvisited   = nodeIsUnvisitedAtCoordinates(x: x - 2, y: y)
        let rightNodeIsUnvisited  = nodeIsUnvisitedAtCoordinates(x: x + 2, y: y)
        let topNodeIsUnvisited    = nodeIsUnvisitedAtCoordinates(x: x, y: y + 2)
        let bottomNodeIsUnvisited = nodeIsUnvisitedAtCoordinates(x: x, y: y - 2)

        /*
         If any of the neighboring nodes are unvisited, return that the node
         has at least one unvisited neighbor node. Otherwise, return that it
         doesn't.
         */
        return leftNodeIsUnvisited || rightNodeIsUnvisited || topNodeIsUnvisited || bottomNodeIsUnvisited
    }

    /// This method checks if a node is unvisited.
    func nodeIsUnvisitedAtCoordinates(x: Int32, y: Int32) -> Bool {
        // Check if a node with the given position exists.
        let nodePosition = int2(x, y)
        guard let node = map.graph.node(atGridPosition: nodePosition) else {
            return false
        }

        // Return if the node is unvisited.
        let nodeIsUnvisited = !visitedNodes.contains(node)

        return nodeIsUnvisited
    }
}
