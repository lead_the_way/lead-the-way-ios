import Foundation
import GameplayKit
import SpriteKit

public class Map {
    /// A grid-based graph representing the navigability space of the map.
    var graph: GKGridGraph<GKGridGraphNode>

    var floor: Floor

    func getGridPath(from start: Point, to end: Point) -> [GKGridGraphNode]? {
        // Calculate a solution path to the map.
        guard let startNode = graph.node(atGridPosition: vector_int2(Int32(start.x), Int32(start.y))),
            let endNode = graph.node(atGridPosition: vector_int2(Int32(end.x), Int32(end.y))) else {
                fatalError("Either start node or end node was nil.")
        }

        return graph.findPath(from: startNode, to: endNode) as? [GKGridGraphNode]
    }

    func getPath(from start: Point, to end: Point) -> [Point]? {
        let solution = getGridPath(from: start, to: end)

        return solution?.map {
            Point(id: 0, x: Int($0.gridPosition.x), y: Int($0.gridPosition.y), obstructionID: 0)
        }
    }

    init(width: Int, height: Int, for floor: Floor) {
        self.floor = floor

        // Initialize the map graph. Remove walls after initialization.
        self.graph = GKGridGraph(fromGridStartingAt: int2(0, 0), width: Int32(width),
                                 height: Int32(height), diagonalsAllowed: false)
        let mapBuilder = MapBuilder(map: self)
        guard let obstructions = floor.obstructions else {
            fatalError("Obstructions not found.") // change this, it is okay if there are no obstructions
        }

        if let startNode = graph.node(atGridPosition: vector_int2(Int32(0), Int32(0))) {
            let mapWalls = mapBuilder.mapWallsForRemoval(startNode: startNode, obstructions: obstructions)

            if DEBUG_MAP {
                // This is a manual "hack" to drastically improve performance.
                // Originally `graph.remove(mapWalls)` took roughly 25,000 milliseoncds.
                // This method takes about 2 milliseconds.
                // This works for routing but DOES NOT WORK when rendering the SceneKit map.
                for node in mapWalls {
                    node.removeConnections(to: node.connectedNodes, bidirectional: true)
                }
            } else {
                graph.remove(mapWalls)
            }
        }
    }
}
