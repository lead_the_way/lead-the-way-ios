import Foundation

/// A representation for a single Todo.
///
/// - Note: A todo object is used for testing the Vapor API.
struct Todo: Codable, Equatable {
    let id: Int
    let title: String
}
