import Foundation

/// A representation for a single Obstruction.
///
/// - Note: An obstruction is a 2D polygonal object (represented by an array of points) that represents something that
///         cannot be traversed.  Examples include walls, pillars, et cetera.
struct Obstruction: Codable {
    let id: Int
    let floorID: Int
    let points: [Point]?
}
