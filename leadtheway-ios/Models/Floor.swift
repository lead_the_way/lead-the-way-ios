import Foundation

/// A representation for a single Floor within a Building.
///
/// - Note: Floors contain the information needed for path finding and user tracking,
///         such as obstructions and destinations.
struct Floor: Codable {
    let id: Int
    let elevation: Double?
    let buildingID: Int
    let name: String?
    let floorNumber: Int?
    let width: Int
    let depth: Int
    let obstructions: [Obstruction]?
    let destinations: [Destination]?
}
