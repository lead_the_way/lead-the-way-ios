import Foundation

/// A representation for a single Point.
///
/// - Note: Points are used as units from which to build obstructions.
struct Point: Codable {
    let id: Int
    let x: Int
    let y: Int
    let obstructionID: Int
}
