import Foundation

/// A representation for a single Destination.
///
/// - Note: A "destination" is a place of interest, e.g. a room, entrance, exit, et cetera.
struct Destination: Codable {
    let id: Int
    let x: Int
    let y: Int
    let floorID: Int
    let description: String?
    let type: DestinationType
}

enum DestinationType: String, Codable {
    case room = "room"
    case entry = "entry"
    case exit = "exit"
    case stairs = "stairs"
    case elevator = "elevator"
    case restroom = "restroom"
    case landmark = "landmark"
    case store = "store"
    case vendingMachine = "vending machine"
    case waterFountain = "water fountain"
    case unknown = "unknown"
}

