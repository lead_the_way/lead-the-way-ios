import Foundation

/// A representation for a single Building.
struct Building: Codable {
    let id: Int
    let name: String?
    let longitude: Double?
    let latitude: Double?
    let floors: [Floor]?
}
