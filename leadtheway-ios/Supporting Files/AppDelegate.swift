import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?

    func application(_ application: UIApplication,
                     supportedInterfaceOrientationsFor window: UIWindow?
        ) -> UIInterfaceOrientationMask {
        // Only allow regular (not upside-down) portrait view throughout the application.
        return UIInterfaceOrientationMask.portrait
    }
}
