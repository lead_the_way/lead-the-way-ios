/// In case of emergency break glass.

/// - Note: If true then lazily remove map wall nodes (disables map rendering)
let DEBUG_MAP = false

/// - Note: If true the load local `getTestFloor()` instead of API.
let DEBUG_API = false

/// - Note: If true then use the SpeechRecognitionEngine.
let DEBUG_SRE = false

/// - Note: If true then the "Debug" button will show on the main view.
let DEBUG_BTN = false
